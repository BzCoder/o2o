import com.bzcoder.o2o.dao.AreaDao;
import com.bzcoder.o2o.entity.Area;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author BaoZhou
 * @date 2018/9/28
 */
public class AreaDaoTest extends BaseTest {
    @Autowired
    private AreaDao areaDao;

    @Test
    public void testQueryArea()
    {
        List<Area> areaList = areaDao.queryArea();
        assertEquals(2,areaList.size());
    }
}
