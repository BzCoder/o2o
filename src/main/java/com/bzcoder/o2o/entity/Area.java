package com.bzcoder.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author BaoZhou
 * @date 2018/9/26
 */
@Data
public class Area {

    /**
     * ID
     */
    private Integer areaId;
    /**
     * 名称
     */
    private String areaName;
    /**
     * 权重
     */
    private Integer priority;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
}
