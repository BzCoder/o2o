package com.bzcoder.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author BaoZhou
 * @date 2018/9/26
 */
@Data
public class WechatAuth {
    private Long wechatAuthid;
    private String openId;
    private PersonInfo personInfo;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
}
