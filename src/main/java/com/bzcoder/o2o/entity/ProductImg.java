package com.bzcoder.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author BaoZhou
 * @date 2018/9/28
 */
@Data
public class ProductImg {
    private Long productImgId;
    private String imgAddr;
    private String imgDesc;
    private Integer priority;
    private Long productId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
}
