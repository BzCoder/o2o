package com.bzcoder.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author BaoZhou
 * @date 2018/9/28
 */
@Data
public class ShopCategory {
    private Long shopCategoryId;
    private String shopCategoryName;
    private String shopCategoryDesc;
    private String shopCategoryImg;
    private String priority;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 父节点
     */
    private ShopCategory parent;

}
