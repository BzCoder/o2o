package com.bzcoder.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author BaoZhou
 * @date 2018/9/28
 */
@Data
public class Shop {
    private Long shopId;
    private String shopName;
    private String shopDesc;
    private String shopAddr;
    private String phone;
    private String shopImg;

    /**
     * 超级管理员给店家的提醒
     */
    private String advice;
    /**
     * 权重 
     */
    private Integer priority;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * -1:不可用 0：审核中 1：可用
     */
    private Integer enableStatus;
    /**
     * 所在区域
     */
    private Area area;
    /**
     * 店主
     */
    private PersonInfo owner;
    /**
     * 店铺类别
     */
    private ShopCategory shopCategory;

}
