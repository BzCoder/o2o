package com.bzcoder.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author BaoZhou
 * @date 2018/9/26
 */
@Data
public class PersonInfo {
    private Long userId;
    private String name;
    private String profileImg;
    private String email;

    /**
     * 0:male 1:female
     */
    private String gender;
    /**
     * 0.disable 1.enable
     */
    private String enableStatus;
    /**
     * 1.customer 2.seller 3.admin
     */
    private Integer userType;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
}
