package com.bzcoder.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 头条
 * @author BaoZhou
 * @date 2018/9/27
 */
@Data
public class HeadLine {
    private Long lineId;
    private String lineName;
    private String lineLink;
    private String lineImg;
    private Integer priority;

    /**
     * 0：不可用 1：可用
     */
    private Integer enableStatus;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
}
