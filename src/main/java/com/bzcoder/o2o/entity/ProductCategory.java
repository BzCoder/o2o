package com.bzcoder.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 商品类别
 * @author BaoZhou
 * @date 2018/9/28
 */
@Data
public class ProductCategory {
    private Long productCategoryId;
    private Long shopId;
    private String productCategoryName;
    private Integer priority;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
}
