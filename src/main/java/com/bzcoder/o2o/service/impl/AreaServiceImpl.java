package com.bzcoder.o2o.service.impl;

import com.bzcoder.o2o.dao.AreaDao;
import com.bzcoder.o2o.entity.Area;
import com.bzcoder.o2o.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author BaoZhou
 * @date 2018/9/29
 */
@Service
public class AreaServiceImpl implements AreaService {
    @Autowired
    AreaDao areaDao;

    public List<Area> getAreaList() {
       return areaDao.queryArea();
    }
}
