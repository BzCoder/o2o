package com.bzcoder.o2o.service;

import com.bzcoder.o2o.entity.Area;

import java.util.List;

/**
 * @author BaoZhou
 * @date 2018/9/28
 */
public interface AreaService {
    List<Area> getAreaList();
}
