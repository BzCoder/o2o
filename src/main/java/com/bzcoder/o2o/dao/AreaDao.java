package com.bzcoder.o2o.dao;

import com.bzcoder.o2o.entity.Area;

import java.util.List;

/**
 * @author BaoZhou
 * @date 2018/9/28
 */
public interface AreaDao {
    List<Area> queryArea();
}
